﻿
//「usermanagement」でデータベース作成
create database usermanagement default character set utf8;

drop table user;

use usermanagement;

//「usermanagement」に「user」というテーブル作成
create table user(
id serial primary key auto_increment,
login_id varchar(255) unique not null,
name varchar(255) unique not null,
birth_date date not null,
password varchar(255) not null,
is_admin boolean not null default false,
create_date datetime not null,
update_date datetime not null
);

//_____________________________________________
//全て入力が必要になるから,idが必要になってしまう
insert into user values(
0,
'admin',
'管理者',
'1995/06/28',
'password',
true,
now(),
now()
);
//______________________________________________


//「user」に管理者データの登録
insert into user (
login_id,
name,
birth_date,
password,
is_admin,
create_date,
update_date
) values(
'admin',
'管理者',
'1995/06/28',
'password',
true,
now(),
now()
);
