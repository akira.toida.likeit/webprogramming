package Beans;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

// Userテーブルの情報を保持するBeans

public class User implements Serializable {
  private int id;
  private String loginId;
  private String name;
  // SQLの年月日を扱うときはDate型
  private Date birthDate;
  private String password;
  private boolean isAdmin;
  // SQLの日時秒を扱うときTimestamp型
  private Timestamp createDate;
  private Timestamp updateDate;


  
  // 全てのデータを残すコンストラクタ

  public User(int id, String loginId, String name, Date birthDate, String password, boolean isAdmin,
      Timestamp createDate, Timestamp updateDate) {
    super();
    this.id = id;
    this.loginId = loginId;
    this.name = name;
    this.birthDate = birthDate;
    this.password = password;
    this.isAdmin = isAdmin;
    this.createDate = createDate;
    this.updateDate = updateDate;
  }
 
  // ログインセッションを残すコンストラクタ
  public User(String loginId, String name, boolean isAdmin) {
  this.loginId = loginId;
  this.name = name;
  this.isAdmin = isAdmin;
}

// ユーザ詳細コンストラクタ
public User(int id, String loginId, String name, Date birthDate, Timestamp createDate,
    Timestamp updateDate) {
  this.id = id;
  this.loginId = loginId;
  this.name = name;
  this.birthDate = birthDate;
  this.createDate = createDate;
  this.updateDate = updateDate;
}

public User() {
  // TODO 自動生成されたコンストラクター・スタブ
}

  /**
   * @return id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id セットする id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return loginId
   */
  public String getLoginId() {
    return loginId;
  }

  /**
   * @param loginId セットする loginId
   */
  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  /**
   * @return name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name セットする name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return birthDate
   */
  public Date getBirthDate() {
    return birthDate;
  }

  /**
   * @param birthDate セットする birthDate
   */
  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  /**
   * @return password
   */
  public String getPassword() {
    return password;
  }

  /**
   * @param password セットする password
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * @return isAdmin
   */
  public boolean isAdmin() {
    return isAdmin;
  }

  /**
   * @param isAdmin セットする isAdmin
   */
  public void setAdmin(boolean isAdmin) {
    this.isAdmin = isAdmin;
  }

  /**
   * @return createDate
   */
  public Timestamp getCreateDate() {
    return createDate;
  }

  /**
   * @param createDate セットする createDate
   */
  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }

  /**
   * @return updateDate
   */
  public Timestamp getUpdateDate() {
    return updateDate;
  }

  /**
   * @param updateDate セットする updateDate
   */
  public void setUpdateDate(Timestamp updateDate) {
    this.updateDate = updateDate;
  }


}
