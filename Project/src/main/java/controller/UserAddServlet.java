package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserAddServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // ログインセッションの有無確認
    HttpSession session = request.getSession();
    User userInfo = (User) session.getAttribute("userInfo");

    if (userInfo == null) {
      // ログインサーブレットへリダイレクト
      response.sendRedirect("LoginServlet");
    }
    else {
    // ユーザー 新規登録画面の表示フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
    dispatcher.forward(request, response);
  }
}

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    // (getParameter()メソッドにformでnameに指定した値を引数にする)
    String loginId = request.getParameter("user-loginid");
    String password = request.getParameter("password");
    String passwordConfilm = request.getParameter("password-confirm");
    String userName = request.getParameter("user-name");
    String birthDate = request.getParameter("birth-date");

    // リクエストパラメータの入力を引数にしてDaoのメソッドを実行
    UserDao userDao = new UserDao();
    User user = userDao.findByLoginId(loginId);

    /** テーブルに該当のデータが見つからなかった場合 * */
    if (!password.equals(passwordConfilm) || loginId.isEmpty() || password.isEmpty()
        || passwordConfilm.isEmpty() || userName.isEmpty() || birthDate.isEmpty() || user == null) {
      
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "入力された内容は正しくありません");
      request.setAttribute("loginId", loginId);
      request.setAttribute("userName", userName);
      request.setAttribute("birthDate", birthDate);

      // ユーザー 新規登録画面にフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
      return;
    }
    
    // 暗号化
    String passwordCode = util.PasswordEncorder.encordPassword(password);

 // リクエストパラメータの入力を引数にしてDaoのメソッドを実行
 // UserDao userDao = new UserDao();
    userDao.UserAdd(userName, loginId, passwordCode, birthDate);

    // ユーザーリストサーブレットにリダイレクト
    response.sendRedirect("UserListServlet");

  }
}
