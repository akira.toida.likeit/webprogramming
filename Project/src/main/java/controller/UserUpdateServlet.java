package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserUpdateServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // ログインセッションの有無確認
    HttpSession session = request.getSession();
    User userInfo = (User) session.getAttribute("userInfo");

    if (userInfo == null) {
      // ログインサーブレットへリダイレクト
      response.sendRedirect("LoginServlet");
    }
    else {
    // リクエストスコープに保存するインスタンス(JavaBeans)の生成
    String id = request.getParameter("id");
    // idをint型に型変換する
    User user = UserDao.findById(Integer.parseInt(id));

    // フィールドにデータをセットしたインスタンスをスコープに追加
    request.setAttribute("user", user);

    // ユーザー更新画面の表示フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
    dispatcher.forward(request, response);
  }
}


  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    // (getParameter()メソッドにformでnameに指定した値を引数にする)
    String loginId = request.getParameter("loginId");
    String Id = request.getParameter("user-id");
    String password = request.getParameter("password");
    String passwordConfilm = request.getParameter("password-confirm");
    String userName = request.getParameter("user-name");
    String birthDate = request.getParameter("birth-date");

    // リクエストパラメータの入力を引数にしてDaoのメソッドを実行
    UserDao userDao = new UserDao();

    if (!password.equals(passwordConfilm) || userName.isEmpty() || birthDate.isEmpty()) {

      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "入力された内容は正しくありません");
      // request.setAttribute("user-id", Id);
      // request.setAttribute("user-name", userName);
      // request.setAttribute("birth-date", birthDate);

      // idをint型に型変換する
      // User user = UserDao.findById(Integer.parseInt(Id));

      User user = new User();
      // 書き換えてる
      user.setLoginId(loginId);
      user.setName(userName);
      // 型変換
      user.setBirthDate(Date.valueOf(birthDate));
      // フィールドにデータをセットしたインスタンスをスコープに追加
      request.setAttribute("user", user);

      // ユーザー更新画面の表示フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
      return;

    } else if (password.isEmpty() && passwordConfilm.isEmpty()) {

      userDao.UserUpdate(Id, userName, birthDate);

      // ユーザーリストサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");
      return;
    }

    // 暗号化
    String passwordCode = util.PasswordEncorder.encordPassword(password);

    // リクエストパラメータの入力を引数にしてDaoのメソッドを実行
    // UserDao userDao = new UserDao();

    userDao.UserUpdateAll(Id, userName, passwordCode, birthDate);

    // // セッションのインスタンスを作成
    // HttpSession session = request.getSession();
    // // セッションからデータを取得
    // User name = (User) session.getAttribute("userInfo");
    // name.setName(userName);
    // session.setAttribute("userInfo", name);

    // ユーザーリストサーブレットにリダイレクト
    response.sendRedirect("UserListServlet");
  }

}
