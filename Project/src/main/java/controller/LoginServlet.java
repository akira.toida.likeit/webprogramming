package controller;


import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Beans.User;
import dao.UserDao;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public LoginServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // TODO 未実装：ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる


    // ログイン画面の表示フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  // ログインボタン押されると実行される
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コード指定(文字化け防止)
    request.setCharacterEncoding("UTF-8");


    // リクエストパラメータの入力項目を取得
    // (getParameter()メソッドにformでnameに指定した値を引数にする)
    String loginId = request.getParameter("user-loginid");
    String password = request.getParameter("password");


    // 暗号化
    String passwordCode = util.PasswordEncorder.encordPassword(password);

    // リクエストパラメータの入力を引数にしてDaoのメソッドを実行
    UserDao userDao = new UserDao();
    boolean admin = false;
    User user = userDao.findByLoginInfo(loginId, passwordCode, admin);

    // ログイン失敗
    // テーブルにデータなし
    if (user == null) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります。");

      // サンプルには記載あり
      // 入力したログインIDを画面に表示するために値をセット
       request.setAttribute("loginId", loginId);

      // ログインjspへフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);
      return;
    }

    // ログイン成功
    // テーブルにデータあり
    // セッションに情報をセット
    HttpSession session = request.getSession();
    session.setAttribute("userInfo", user);

    // ユーザーリストサーブレットにリダイレクト
    response.sendRedirect("UserListServlet");

  }
}
