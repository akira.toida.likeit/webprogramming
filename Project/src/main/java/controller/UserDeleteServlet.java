package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // ログインセッションの有無確認
      HttpSession session = request.getSession();
      User userInfo = (User) session.getAttribute("userInfo");

      if (userInfo == null) {
        // ログインサーブレットへリダイレクト
        response.sendRedirect("LoginServlet");
      }
      else {

      // リクエストスコープに保存するインスタンス(JavaBeans)の生成
      String id = request.getParameter("id");

      // idをint型に型変換する
      User user = UserDao.findById(Integer.parseInt(id));

      // フィールドにデータをセットしたインスタンスをスコープに追加
      request.setAttribute("user", user);

      // ユーザー詳細画面の表示フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
      dispatcher.forward(request, response);
	}
  }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      // (getParameter()メソッドにformでnameに指定した値を引数にする)
      String Id = request.getParameter("user-id");

      // リクエストパラメータの入力を引数にしてDaoのメソッドを実行
      UserDao userDao = new UserDao();
      userDao.UserDelete(Id);

      // ユーザーリストサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");
	}

}
