package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import Beans.User;

public class UserDao {

  public User findByLoginInfo(String loginId, String password, boolean isAdmin) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBmanager.getConnection();

      // TODO ここに処理を書いていく
      // ログインできるかチェックするSQL
      String sql = "select * from user where login_id = ? and password = ?";

      // SELECT文を実行し結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // ログイン失敗
      if (!rs.next()) {
        return null;
      }

      // ログイン成功
      String loginIdData = rs.getString("login_id");
      String nameData = rs.getString("name");
      boolean admin = rs.getBoolean("is_admin");
      return new User(loginIdData, nameData, admin);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }



  public void UserAdd(String name, String loginId, String password, String birthDate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBmanager.getConnection();

      // TODO ここに処理を書いていく
      // 登録するSQL INSERT文を準備
      String sql =
          "insert into user (login_id,name,birth_date,password,is_admin,create_date,update_date) values (?,?,?,?,false,now(),now())";

      // INSERT文を実行し結果表を登録
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, name);
      pStmt.setString(3, birthDate);
      pStmt.setString(4, password);
      pStmt.executeUpdate();

      // } catch (SQLIntegrityConstraintViolationException e) {
      // e.printStackTrace();
    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }



  // 複数のデータをリストにしているから戻り値はリスト
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBmanager.getConnection();

      // 管理者以外のSELECT文
      String sql = "select * from user where is_admin = '0'";

      // SELECT文を実行し結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 格納されたレコードをUser型のインスタンスに変換し、userListにaddで追加する
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }



  public List<User> userSearch(String searchLoginId, String searchName, String beginningBirthDate,
      String endBirthDate) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBmanager.getConnection();

      // 管理者以外のSELECT文
      StringBuilder stringBuilder = new StringBuilder("select * from user where is_admin = '0'");

      // and login_id = ? and name like %?% and birth_date >= ? and birth_date <= ?
      List<String> searchList = new ArrayList<String>();

      if (!searchLoginId.isEmpty()) {
        stringBuilder.append(" and login_id = ?");
        // リスト作ってリストの中にaddでsearchLoginIdをいれる
         searchList.add(searchLoginId);
      }

      if (!searchName.isEmpty()) {
        stringBuilder.append(" and name like ?");
        //setStringで"%"
        //
        searchList.add("%" + searchName + "%");
      }

      if (!beginningBirthDate.isEmpty()) {
        stringBuilder.append(" and birth_date >= ?");
        searchList.add(beginningBirthDate);
      }

      if (!endBirthDate.isEmpty()) {
        stringBuilder.append(" and birth_date <= ?");
        searchList.add(endBirthDate);
      }

      // SELECT文を実行し結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(stringBuilder.toString());

      // まだ
      // for文でリストの回数setStringする
      // if文の順番で
      for (int i = 0; i < searchList.size(); i++) {
        pStmt.setString(i + 1, searchList.get(i));
      }

      // for (String user : searchList) {
      // pStmt.setString(searchList.size(), user);
//         pStmt.setString(1, searchLoginId);
//       pStmt.setString(2, "%" + searchName + "%");
//       pStmt.setString(3, beginningBirthDate);
//       pStmt.setString(4, endBirthDate);
      // }

       
   
      ResultSet rs = pStmt.executeQuery();


      // 格納されたレコードをUser型のインスタンスに変換し、userListにaddで追加する
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }



  public static User findById(int id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBmanager.getConnection();

      // 登録するSQL SELECT文を準備
      String sql = "  select * from user where id = ?";

      // SELECT文を実行し結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      ResultSet rs = pStmt.executeQuery();

      // 失敗
      if (!rs.next()) {
        return null;
      }

      // 必要な情報をUserクラスにして返す
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");

      return new User(id, loginId, name, birthDate, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return null;
  }



  public void UserUpdateAll(String id, String name, String password, String birthDate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBmanager.getConnection();

      // 登録するSQL update文を準備
      String sql = "update user set password = ? , name = ? , birth_date = ? where id = ?";

      // update文を実行し結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, password);
      pStmt.setString(2, name);
      pStmt.setString(3, birthDate);
      pStmt.setString(4, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void UserUpdate(String id, String name, String birthDate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBmanager.getConnection();

      // 登録するSQL update文を準備
      String sql = "update user set name = ? , birth_date = ? where id = ?";

      // update文を実行し結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setString(2, birthDate);
      pStmt.setString(3, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



  public void UserDelete(String id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBmanager.getConnection();

      // 登録するSQL delete文を準備
      String sql = "delete from user where id = ?";

      // delete文を実行し結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }


  // ユーザ新規登録例外処理(既に登録されているログインID)
  public User findByLoginId(String loginId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBmanager.getConnection();

      // TODO ここに処理を書いていく
      // ログインできるかチェックするSQL
      String sql = "select * from user where login_id = ?";

      // SELECT文を実行し結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();

      // 既に登録されている
      if (rs.next()) {
        return null;
      }

      // 登録されていない
      return new User();

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }



  public User findByLoginName(String userName) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBmanager.getConnection();

      // TODO ここに処理を書いていく
      // ログインできるかチェックするSQL
      String sql = "select * from user where name = ?";

      // SELECT文を実行し結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userName);
      ResultSet rs = pStmt.executeQuery();

      // 既に登録されている
      if (rs.next()) {
        return null;
      }

      // 登録されていない
      return new User();

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }



}
